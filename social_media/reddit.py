"""Gets data from Reddit."""
from sys import path as sys_path
from time import time
from os import path

from praw import Reddit
# from praw.reddit import subreddits

from . import CLIENT_ID, CLIENT_SECRET, CLIENT_AGENT


class RedditCrawler():
    """Crawl reddit api using praw."""

    _credentials = {
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'user_agent': CLIENT_AGENT
    }

    def __init__(self, *args, **kwargs):
        """Initialize reddit instance."""
        self.instance = Reddit(**self._credentials)

    def _crypto_subreddits(self, *args, **kwargs):
        """Generate a list of subreddits."""
        currency = kwargs.pop('currency', None)
        time_filter = kwargs.get('time_filter', 'year')
        query = f'self:yes'
        current_timestamp = time()
        two_months_timestamp = current_timestamp - (60 * 60 * 24 * 60)
        # query = f'timestamp:{current_timestamp}..{two_months_timestamp}'
        _subreddit = self.instance.subreddit(currency)
        return _subreddit.search(
            query,
            sort='new',
            time_filter=time_filter,
            syntax='cloudsearch',
            limit=1000
            )


class RedditFileReader():
    """DEPRECATED: Does not read inline
    
    Reads from a file and tries to read.
    """

    INPUT_DIR = path.join(sys_path[0], 'input_data')

    def __init__(self, filename, *args, **kwargs):
        """Initialize reader.

        Give the filename of the file that is in the
        input_data directory.
        """
        self.filename = filename

    def __enter__(self):
        """Open a file and return it."""
        abs_filename = path.join(self.INPUT_DIR, self.filename)
        self.file = open(abs_filename)
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Close the file."""
        self.file.close()
