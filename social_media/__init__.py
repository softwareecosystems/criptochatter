"""Constants."""
from os import environ
from functools import wraps
from time import time, ctime
import traceback

CLIENT_ID = environ.get('CLIENT_ID', None)
CLIENT_SECRET = environ.get('SECRET_KEY', None)
CLIENT_AGENT = environ.get('USER_AGENT', None)
APP_SECRET = environ.get('APP_SECRET', None)


def timeit(f):
    """Define wrapper for timming a function."""
    @wraps(f)
    def wrapper(*args, **kwargs):
        """Get the time before and after execution and substract."""
        ts = time()
        result = None
        print("function: {}\nwith args:[{}, {}] started at {}".format(
            f.__name__,
            args,
            kwargs,
            ctime())
            )
        try:
            result = f(*args, **kwargs)
        except Exception as e:
            print("function raised an unexpected Exception at {}\n"
                  "-----------------Exception-----------------\n{}\n"
                  "-------------------Stack-------------------\n{}\n".format(
                    ctime(),
                    e,
                    traceback.print_tb(e.__traceback__)
                    )
                  )
        else:
            te = time()
            print("function: {}\nwith args:[{}, {}]\ntook: {:2.2f}"
                  " seconds".format(
                    f.__name__,
                    args,
                    kwargs,
                    te-ts
                    ))
            print("ended at {}".format(ctime()))
            return result
    return wrapper
