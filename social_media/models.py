"""Model tables in a database.

TODO:
  * Override the get_or_create method of the models that have a score
  *
"""
from peewee import (
    Model,
    SqliteDatabase,
    CharField,
    IntegerField,
    ForeignKeyField,
    DateTimeField,
    IntegrityError,
)
from praw.models.reddit.redditor import Redditor as PRAWRedditor
from prawcore.exceptions import NotFound


db = SqliteDatabase("crypto.db")


class BaseModel(Model):
    """Generic methods for all the models in the library."""

    @classmethod
    def classic_get_or_create(cls, **kwargs):
        """"""
        defaults = kwargs.pop('defaults', {})
        query = cls.select()
        for field, value in kwargs.items():
            query = query.where(getattr(cls, field) == value)

        try:
            return query.get(), False
        except cls.DoesNotExist:
            try:
                if defaults:
                    kwargs.update(defaults)
                with cls._meta.database.atomic():
                    return cls.create(**kwargs), True
            except IntegrityError as exc:
                try:
                    return query.get(), False
                except cls.DoesNotExist:
                    raise exc

    @classmethod
    def get_or_create(cls, query, defaults):
        """Get or create a model based on the query.

        When checking for the queries:
            * exclude the score.
            * author if it is None
        Raises an exception if the creation fails.
        """
        try:
            return query.get(), False
        except cls.DoesNotExist:
            try:
                with cls._meta.database.atomic():
                    return cls.create(**defaults), True
            except IntegrityError as exc:
                try:
                    return query.get(), False
                except cls.DoesNotExist:
                    raise exc

    class Meta:
        """Ensure that all models use the same database."""

        database = db


class Author(BaseModel):
    """The owner of reddit."""

    name = CharField()
    uid = CharField(unique=True)

    @classmethod
    def get_or_create(cls, **kwargs):
        """Override to get a redditor.

        Takes better care of retrieving data and uploading to database.
        """
        defaults = kwargs.pop('defaults', {})
        query = cls.select()
        praw_author = kwargs.get('author')
        if not praw_author or not isinstance(praw_author, PRAWRedditor):
            raise Exception('{} needs to be a praw author'.format(praw_author))
        try:
            # this sometimes raises a prawncore NotFound exception on an id.
            if hasattr(praw_author, 'id') and praw_author.id:
                query = query.where(getattr(cls, 'uid') == praw_author.id)
                defaults['uid'] = praw_author.id
        except NotFound:
            pass
        if hasattr(praw_author, 'name') and praw_author.name:
            query = query.where(getattr(cls, 'name') == praw_author.name)
            defaults['name'] = praw_author.name
        # Need to pass the defaults in this case because kwargs does not
        # contain name nor uid, it contains a praw's redditor object.
        if not defaults.get('uid', None):
            defaults['uid'] = praw_author.name
        return super().get_or_create(query, defaults)


class Submission(BaseModel):
    """Represents a subreddit submission."""

    sub_id = CharField(unique=True)
    subreddit = CharField()
    title = CharField()
    author = ForeignKeyField(Author, backref='submissions')
    body = CharField()
    score = IntegerField(null=True)
    submitted = DateTimeField()

    @classmethod
    def get_or_create(cls, **kwargs):
        """Get or create a submission.

        Special care of cases where invalid kwargs are passed to the query
        """
        defaults = kwargs.pop('defaults', {})
        query = cls.select()
        for field, value in kwargs.items():
            if isinstance(value, PRAWRedditor):
                _author, _ = Author.get_or_create(author=value)
            if field == 'score':
                defaults['score'] = value
                continue
            if field in ['title', 'body']:
                value = value.replace('\n', ' ')
            if field == 'sub_id':
                query = query.where(getattr(cls, field) == value)
        if defaults:
            kwargs.update(defaults)
        kwargs['author'] = _author
        return super().get_or_create(query, kwargs)


class Comment(BaseModel):
    """Comment that are made by redditors."""

    comment_id = CharField(unique=True)
    author = ForeignKeyField(Author, backref='comments', null=True)
    submission = ForeignKeyField(Submission, backref='comments')
    parent = CharField(null=True)
    # not saving the self referencing foreign key...
    # parent = ForeignKeyField('self', backref='parent', null=True)
    body = CharField()
    created = DateTimeField()
    score = IntegerField(null=True)

    @classmethod
    def get_or_create(cls, **kwargs):
        """Get or create a comment.

        Handle special cases when kwargs given are invalid.
        May need to search specifically for commend id and then try this...
        """
        defaults = kwargs.pop('defaults', {})
        query = cls.select()
        _author = None
        for field, value in kwargs.items():
            if isinstance(value, PRAWRedditor):
                _author, _ = Author.get_or_create(author=value)
            if field == 'score':
                defaults['score'] = value
                continue
            if field == 'body':
                value = value.replace('\n', ' ')
            if field == 'comment_id':
                # only look for the id. if it doesnt exist, then create it.
                query = query.where(getattr(cls, field) == value)
        if defaults:
            kwargs.update(defaults)
        kwargs['author'] = _author
        return super().get_or_create(query, kwargs)


def create_tables():
    """Call only once to create the database."""
    with db:
        db.create_tables([Author, Submission, Comment])
