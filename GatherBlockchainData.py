#!/usr/bin/env python
"""Get reddit data and store it in a database."""

from json import loads
from sys import path as sys_path
from os import path

from social_media import timeit
from social_media.reddit import RedditCrawler
from social_media.models import Submission, Comment, Author
from datetime import datetime


def get_date(utcdatetime):
    """Get a date from a utc datetime."""
    return datetime.utcfromtimestamp(utcdatetime)


def get_replies(submission, replies, parent):
    """Recursively get all replies.

    Note that replies are simply comments that have a parent.
    """
    # reply could be a class of MoreComments...
    for reply in replies:
        if hasattr(reply, 'body'):
            if reply.body == '[deleted]' or reply.body == '[removed]':
                # reply could be a class of MoreComments...
                if hasattr(reply, 'comments'):
                    get_replies(submission, reply.comments(), parent)
            else:
                if reply.replies:
                    _reply, _ = Comment.get_or_create(
                        comment_id=reply.id,
                        author=reply.author,
                        body=reply.body,
                        submission=submission,
                        parent=parent,
                        created=get_date(reply.created),
                        score=reply.score
                        )
                    # recursively get more replies
                    get_replies(submission, reply.replies, parent=_reply.id)
                else:
                    _reply, _ = Comment.get_or_create(
                        comment_id=reply.id,
                        author=reply.author,
                        body=reply.body,
                        submission=submission,
                        parent=parent,
                        created=get_date(reply.created),
                        score=reply.score
                        )
        else:
            # if the reply has comments, then get those comments.
            if hasattr(reply, 'comments'):
                get_replies(submission, reply.comments(), parent)


def get_comments(submission, comments):
    """Recursively get comments.

    :comment: Praw comment object
    :returns: a child comment.
    """
    for comment in comments:
        if hasattr(comment, 'body') and\
           comment.body != '[deleted]' and\
           comment.body != '[removed]':
            _comment, _ = Comment.get_or_create(
                comment_id=comment.id,
                author=comment.author,
                body=comment.body,
                submission=submission,
                created=get_date(comment.created),
                score=comment.score
                )
            for reply in comment.replies:
                get_replies(submission, comment.replies, _comment.id)
        elif hasattr(comment, 'comments'):
            get_comments(submission, comment.comments())


@timeit
def get_currency_data(reddit, currencies, time_filter='day'):
    """Get currency data."""
    for currency in currencies:
        submissions = reddit._crypto_subreddits(
            currency=currency,
            time_filter=time_filter
            )
        # submissions = reddit.get_interval(currency=currency)
        for submission in submissions:
            _submission, _ = Submission.get_or_create(
                sub_id=submission.id,
                title=submission.title,
                body=submission.selftext,
                author=submission.author,
                submitted=get_date(submission.created_utc),
                subreddit=submission.subreddit.display_name,
                score=submission.score
                )
            # get_comments(_submission, submission.comments)


def get_from_PRAW():
    """Get data using PRAW."""
    reddit = RedditCrawler()
    currencies = [
        'CrowdMachine',
        'TheBigXP',
        'JibrelNetwork',
        'INS_Ecosystem',
        'PowerLedger',
        'SubstratumNetwork',
        'FunfairTech',
        'waltonchain',
        'Moreno',
        'NEO',
        'ethereum',
        'ripple',
        'bitcoin'
        ]
    get_currency_data(reddit, currencies, time_filter='year')


def read_submissions_into_db(subreddit, data_dict):
    """Read data into the database."""
    author, _ = Author.classic_get_or_create(
        name=data_dict.get('author'),
        defaults={'uid': data_dict.get('author')}
        )
    # hoping not to get duplicates...
    submission, _ = Submission.classic_get_or_create(
        sub_id=data_dict.get('id'),
        title=data_dict.get('title'),
        body=data_dict.get('selftext'),
        author=author,
        submitted=get_date(data_dict.get('created_utc')),
        subreddit=subreddit,
        defaults={'score': data_dict.get('score')}
        )


@timeit
def get_from_dataset(filenames, subredditis):
    """Get data from a file."""
    INPUT_DIR = path.join(sys_path[0], 'input_data')

    for _file in filenames:
        abs_filename = path.join(INPUT_DIR, _file)
        print(f'processing {abs_filename}\n--------------------------------')
        if path.exists(abs_filename):
            with open(abs_filename) as file_object:
                for line in file_object:
                    data_dict = loads(line)
                    _subreddit = data_dict.get('subreddit', '').lower()
                    is_self = data_dict.get('is_self', False)
                    if is_self and _subreddit in subreddits:
                        read_submissions_into_db(_subreddit, data_dict)
        else:
            print("File does not exists. Continuing with the rest.")


def read_comments_into_db(subreddit, data_dict):
    """Based on the data dict, create comments and its links."""
    # author, _ = Author.classic_get_or_create


def read_comments(filenames, subreddits):
    """Read comments into database."""


if __name__ == '__main__':
    # get_from_PRAW()

    submission_filenames = [
        # 'RS_2017-06',
        # 'RS_2017-07',
        # 'RS_2017-08',
        # 'RS_2017-09',
        # 'RS_2017-10',
        # 'RS_2017-11',
        # 'RS_2017-12',
        # 'RS_2018-01',
        # 'RS_2018-02',
        ]
    subreddits = [
        'monero',
        'thebigxp',
        'jibrelnetwork',
        'ins_ecosystem',
        'powerledger',
        'substratumnetwork',
        'funfairtech',
        'waltonchain',
        'moreno',
        'neo',
        'ethereum',
        'ripple',
        'bitcoin'
        ]
    get_from_dataset(submission_filenames, subreddits)
